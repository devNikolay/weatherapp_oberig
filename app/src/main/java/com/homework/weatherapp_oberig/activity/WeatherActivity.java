package com.homework.weatherapp_oberig.activity;

import android.os.Bundle;

import com.homework.weatherapp_oberig.R;
import com.homework.weatherapp_oberig.fragments.BaseFragment;
import com.homework.weatherapp_oberig.fragments.WeatherFragment;

public class WeatherActivity extends BaseActivity {

    @Override
    protected BaseFragment getInitFragment() {
        return new WeatherFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        changeFragment(getInitFragment(), false);
    }
}
