package com.homework.weatherapp_oberig.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.homework.weatherapp_oberig.R;
import com.homework.weatherapp_oberig.gps.GPSTracker;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    @Bind(R.id.btn_getWeather)
    Button mGetWeather;
    private GPSTracker mGpsTracker;

    private static final int TIME_DELAY = 2000;
    private static long back_pressed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGetWeather.setOnClickListener(v -> {
            if (mGpsTracker.getIsGPSTrackingEnabled()) {
                Intent intent = new Intent(getApplicationContext(), WeatherActivity.class);
                startActivity(intent);
            } else {
                mGpsTracker.showSettingsAlert();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGpsTracker = new GPSTracker(this);
        mMap = googleMap;
        mMap.setOnMyLocationChangeListener(location -> setupMap());
        if (mGpsTracker.getIsGPSTrackingEnabled()) {
            setupMap();
        } else {
            mGpsTracker.showSettingsAlert();
        }
    }

    private void setupMap() {
        double mLat = mGpsTracker.getmLatitude();
        double mLot = mGpsTracker.getmLongitude();

        LatLng myLocation = new LatLng(mLat, mLot);
        String city = mGpsTracker.getLocality(this);
        String country = mGpsTracker.getCountryName(this);
        MarkerOptions markerOptions = new MarkerOptions();
        Marker locationMarker = mMap.addMarker(markerOptions.position(myLocation).title("You are in " + city + ", " + country));
        locationMarker.setDraggable(true);
        locationMarker.showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));

    }

    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(getBaseContext(), "Press once again to exit!",
                    Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }
}
