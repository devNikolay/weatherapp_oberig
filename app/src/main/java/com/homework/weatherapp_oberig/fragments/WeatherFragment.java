package com.homework.weatherapp_oberig.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.homework.weatherapp_oberig.R;
import com.homework.weatherapp_oberig.gps.GPSTracker;
import com.homework.weatherapp_oberig.network.NetworkManager;
import com.homework.weatherapp_oberig.network.NetworkService;
import com.homework.weatherapp_oberig.pojos.WeatherAll;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class WeatherFragment extends BaseFragment {

    private static final String IMAGE_URL = "http://api.openweathermap.org/img/w/";
    private static final String API_KEY = "778063581d090e8c19653557f2ec9476";
    private static final String UNITS = "metric";

    @Bind(R.id.tv_CityName)
    TextView mCityName;
    @Bind(R.id.tv_CountryName)
    TextView mCountryName;
    @Bind(R.id.textView)
    TextView textView;
    @Bind(R.id.tv_latitude)
    TextView mLatitude;
    @Bind(R.id.tv_longitude)
    TextView mLongitude;
    @Bind(R.id.tv_tempNow)
    TextView mTempNow;
    @Bind(R.id.tv_tempMin)
    TextView mTempMin;
    @Bind(R.id.tv_tempMax)
    TextView mTempMax;
    @Bind(R.id.tv_seaLvl)
    TextView mSeaLvl;
    @Bind(R.id.tv_grndLvl)
    TextView mGrndLvl;
    @Bind(R.id.tv_weatherDescription)
    TextView mWeatherDescription;
    @Bind(R.id.tv_windSpeed)
    TextView mWindSpeed;
    @Bind(R.id.tv_windDeg)
    TextView mWindDeg;
    @Bind(R.id.tv_dt)
    TextView mDateTime;
    @Bind(R.id.tv_sunrise)
    TextView mSunrise;
    @Bind(R.id.tv_sunset)
    TextView mSunset;
    @Bind(R.id.iv_weatherIcon)
    ImageView mWeatherIcon;

    private Subscription mSubscription;
    private CompositeSubscription compositeSubscription;

    private Observable<WeatherAll> mObserver;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NetworkService service = NetworkManager.getWeatherService();
        GPSTracker mGpsTracker = new GPSTracker(getContext());
        compositeSubscription = new CompositeSubscription();
        double mLat = mGpsTracker.getmLatitude();
        double mLot = mGpsTracker.getmLongitude();
        mObserver = service.getWeather(mLat, mLot, UNITS, API_KEY);
        mSubscription =  mObserver.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::bindWeather);
        compositeSubscription.add(mSubscription);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        ButterKnife.bind(this, view);
        textView.setVisibility(View.INVISIBLE);
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void bindWeather(WeatherAll weatherAll) {
        textView.setVisibility(View.VISIBLE);
        mCityName.setText(weatherAll.getName());
        mCountryName.setText(weatherAll.getSys().getCountry());
        mLatitude.setText("lat " + weatherAll.getMCoordinates().getLat());
        mLongitude.setText("lon " + weatherAll.getMCoordinates().getLong());
        mTempNow.setText("temp now " + weatherAll.getMainData().getTemp());
        mTempMax.setText("temp max " + weatherAll.getMainData().getTempMax());
        mTempMin.setText("temp min " + weatherAll.getMainData().getTempMin());
        mSeaLvl.setText("Sea lvl " + weatherAll.getMainData().getSeaLevel());
        mGrndLvl.setText("Grnd lvl " + weatherAll.getMainData().getGrndLevel());
        mWeatherDescription.setText(weatherAll.getMWeather().get(0).getDescription());
        mWindSpeed.setText("Wind speed " + weatherAll.getWind().getSpeed());
        mWindDeg.setText("Wind deg " + weatherAll.getWind().getDeg());
        mDateTime.setText("dt " + weatherAll.getDt());
        mSunrise.setText("Sunrise " + String.valueOf(weatherAll.getSys().getSunrise()));
        mSunset.setText("Sunset " + String.valueOf(weatherAll.getSys().getSunset()));
        String pathIcon = IMAGE_URL + weatherAll.getMWeather().get(0).getIcon() + ".png";
        loadWeatherPic(pathIcon);
    }

    private void loadWeatherPic(String pathIcon) {
        Picasso.with(getContext())
                .load(pathIcon)
                .into(mWeatherIcon);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        if(compositeSubscription!=null){
            compositeSubscription.unsubscribe();
        }
        super.onDestroyView();
    }
}
