package com.homework.weatherapp_oberig.pojos;

import com.google.gson.annotations.SerializedName;

import lombok.experimental.Accessors;

@lombok.Data
@Accessors(prefix = "m")
public class Weather {
    @SerializedName("id") private int mId;
    @SerializedName("main") private String mName;
    @SerializedName("description") private String mDescription;
    @SerializedName("icon") private String mIcon;

}
