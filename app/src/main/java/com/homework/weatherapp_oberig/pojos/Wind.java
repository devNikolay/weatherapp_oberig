package com.homework.weatherapp_oberig.pojos;

import com.google.gson.annotations.SerializedName;

import lombok.experimental.Accessors;

@lombok.Data
@Accessors(prefix = "m")
public class Wind {
    @SerializedName("speed") private String mSpeed;
    @SerializedName("deg") private String mDeg;

}
