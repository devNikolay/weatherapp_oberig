package com.homework.weatherapp_oberig.pojos;

import com.google.gson.annotations.SerializedName;

import lombok.experimental.Accessors;

@lombok.Data
@Accessors(prefix = "m")
public class MainData {
    @SerializedName("temp") private String mTemp;
    @SerializedName("pressure") private String mPressure;
    @SerializedName("humidity") private int mHumidity;
    @SerializedName("temp_min") private String mTempMin;
    @SerializedName("temp_max") private String mTempMax;
    @SerializedName("sea_level") private String mSeaLevel;
    @SerializedName("grnd_level") private String mGrndLevel;
}
