package com.homework.weatherapp_oberig.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.experimental.Accessors;

@lombok.Data
@Accessors(prefix = "m")
public class WeatherAll {
    @SerializedName("coord")
    @Accessors private Coordinates mCoordinates;

    @SerializedName("weather")
    @Accessors private List<Weather> mWeather = new ArrayList<>();

    @SerializedName("base") private String mBase;
    @SerializedName("main") private MainData mMainData;
    @SerializedName("wind") private Wind mWind;

    @SerializedName("clouds")
    @Accessors private Clouds mClouds;

    @SerializedName("dt") private String mDt;
    @SerializedName("sys") private Sys mSys;
    @SerializedName("id") private String mId;
    @SerializedName("name") private String mName;
    @SerializedName("cod") private  String mCod;
}

