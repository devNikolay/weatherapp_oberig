package com.homework.weatherapp_oberig.pojos;

import com.google.gson.annotations.SerializedName;

import lombok.experimental.Accessors;

@lombok.Data
@Accessors(prefix = "m")
public class Clouds {
    @SerializedName("all") private String mAll;
}
