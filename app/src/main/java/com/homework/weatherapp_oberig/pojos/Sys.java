package com.homework.weatherapp_oberig.pojos;

import com.google.gson.annotations.SerializedName;

import lombok.experimental.Accessors;

@lombok.Data
@Accessors(prefix = "m")
public class Sys {
    @SerializedName("message") private String mMassage;
    @SerializedName("country") private String mCountry;
    @SerializedName("sunrise") private int mSunrise;
    @SerializedName("sunset") private int mSunset;

}
