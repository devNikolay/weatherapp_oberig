package com.homework.weatherapp_oberig.network;


import com.homework.weatherapp_oberig.pojos.WeatherAll;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface NetworkService {
    @GET("/data/2.5/weather")
    Observable<WeatherAll> getWeather(@Query("lat") double lat,
                                      @Query("lon") double lon,
                                      @Query("units") String units,
                                      @Query("appid") String appId);
}

